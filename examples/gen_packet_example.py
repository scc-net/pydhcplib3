#!/usr/bin/python

import logging
logging.basicConfig()

from pydhcplib.dhcp_packet import DhcpPacket
from pydhcplib.type_ipv4 import ipv4


packet = DhcpPacket()

packet.SetOption("op", [1])
packet.SetOption("domain_name", b"anemon.org")
packet.SetOption("router", ipv4("192.168.0.1").list() + [6, 4, 2, 1])
packet.SetOption("time_server", [100, 100, 100, 7, 6, 4, 2, 1])
packet.SetOption("yiaddr", [192, 168, 0, 18])
packet.SetOption("xid", [0, 0, 0, 123])
packet.SetOption("chaddr", [0, 0, 0, 123, 12, 13] + [0] * (16 - 6))
packet.AddLine("secs: 12")

print(packet.str())
bytedata = packet.EncodePacket()

packet2 = DhcpPacket()
packet2.DecodePacket(bytedata, True)
print(packet2.str())
