# pydhcplib
# Copyright (C) 2008 Mathieu Ignacio -- mignacio@april.org
#
# This file is part of pydhcplib.
# Pydhcplib is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from binascii import unhexlify, hexlify

from typing import List, Union

# the int method shadows the builtin int
import builtins

# Check and convert hardware/nic/mac address type
class hwmac:
    def __init__(
        self, value: Union[str, bytes, List[int]] = "00:00:00:00:00:00"
    ) -> None:
        self._hw_numlist = []  # type: List[int]
        self._hw_string = ""
        hw_type = type(value)
        if isinstance(value, str):
            value = value.strip()
            self._hw_string = value
            self._StringToNumlist()
            self._CheckNumList()
        elif isinstance(value, list):
            self._hw_numlist = value
            self._CheckNumList()
            self._NumlistToString()
        elif isinstance(value, bytes):
            self._hw_numlist = list(value)
            self._CheckNumList()
            self._NumlistToString()
        else:
            raise TypeError("hwmac init : Valid types are str, bytes and list")

    # Check if _hw_numlist is valid and raise error if not.
    def _CheckNumList(self) -> None:
        if len(self._hw_numlist) != 6:
            raise ValueError("hwmac : wrong list length")
        for part in self._hw_numlist:
            if not isinstance(part, int):
                raise TypeError("hwmac : each element of list must be int")
            if part < 0 or part > 255:
                raise ValueError("hwmac : need numbers between 0 and 255.")

    def _StringToNumlist(self) -> None:
        self._hw_string = self._hw_string.replace("-", ":").replace(".", ":")
        self._hw_string = self._hw_string.lower()

        for twochar in self._hw_string.split(":"):
            self._hw_numlist.append(ord(unhexlify(twochar)))

    # Convert NumList type ip to String type ip
    def _NumlistToString(self) -> None:
        self._hw_string = ":".join("{:02x}".format(c) for c in self._hw_numlist)

    # Convert String type ip to NumList type ip
    # return ip string
    def str(self) -> builtins.str:
        return self._hw_string

    # return ip list (useful for DhcpPacket class)
    def list(self) -> List[int]:
        return self._hw_numlist

    def __hash__(self) -> int:
        return self._hw_string.__hash__()

    def __repr__(self) -> builtins.str:
        return self._hw_string

    def __eq__(self, other: object) -> bool:
        return self._hw_string == other

    def __bool__(self) -> bool:
        if self._hw_string != "00:00:00:00:00:00":
            return True
        return False
