# pydhcplib
# Copyright (C) 2008 Mathieu Ignacio -- mignacio@april.org
#
# This file is part of pydhcplib.
# Pydhcplib is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import logging
import socket
import select

from typing import Optional

from . import dhcp_packet

logger = logging.getLogger(__name__)


class DhcpNetwork:
    def __init__(self, listen_address="0.0.0.0", listen_port=67, emit_port=68) -> None:

        self.listen_port = int(listen_port)
        self.emit_port = int(emit_port)
        self.listen_address = listen_address
        self.so_reuseaddr = False
        self.so_broadcast = True
        self.dhcp_socket = None  # type: socket.socket

    # Networking stuff
    def CreateSocket(self) -> None:
        self.dhcp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        if self.so_broadcast:
            self.dhcp_socket.setsockopt(
                socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        if self.so_reuseaddr:
            self.dhcp_socket.setsockopt(
                socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def EnableReuseaddr(self) -> None:
        self.so_reuseaddr = True

    def DisableReuseaddr(self) -> None:
        self.so_reuseaddr = False

    def EnableBroadcast(self) -> None:
        self.so_broadcast = True

    def DisableBroadcast(self) -> None:
        self.so_broadcast = False

    def BindToDevice(self) -> None:
        self.dhcp_socket.setsockopt(
            socket.SOL_SOCKET, socket.SO_BINDTODEVICE, self.listen_address + "\0"
        )

        self.dhcp_socket.bind(("", self.listen_port))

    def BindToAddress(self) -> None:
        self.dhcp_socket.bind((self.listen_address, self.listen_port))

    def GetNextDhcpPacket(self, timeout=60) -> Optional[dhcp_packet.DhcpPacket]:
        while True:

            print("select", self.dhcp_socket)
            data_input, data_output, data_except = select.select(
                [self.dhcp_socket], [], [], timeout
            )
            print("endselect", data_input)

            if data_input != []:
                (data, source_address) = self.dhcp_socket.recvfrom(2048)
            else:
                return None

            print(data)
            if data == b"":
                return None

            packet = dhcp_packet.DhcpPacket()
            packet.source_address = source_address
            print("decode", data)
            packet.DecodePacket(data)

            self.HandleDhcpAll(packet)

            if packet.IsDhcpDiscoverPacket():
                self.HandleDhcpDiscover(packet)
            elif packet.IsDhcpRequestPacket():
                self.HandleDhcpRequest(packet)
            elif packet.IsDhcpDeclinePacket():
                self.HandleDhcpDecline(packet)
            elif packet.IsDhcpReleasePacket():
                self.HandleDhcpRelease(packet)
            elif packet.IsDhcpInformPacket():
                self.HandleDhcpInform(packet)
            elif packet.IsDhcpOfferPacket():
                self.HandleDhcpOffer(packet)
            elif packet.IsDhcpAckPacket():
                self.HandleDhcpAck(packet)
            elif packet.IsDhcpNackPacket():
                self.HandleDhcpNack(packet)
            else:
                self.HandleDhcpUnknown(packet)

                return packet

    def SendDhcpPacketTo(self, packet, _ip, _port) -> int:
        return self.dhcp_socket.sendto(packet.EncodePacket(), (_ip, _port))

    # Server side Handle methods
    def HandleDhcpDiscover(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpRequest(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpDecline(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpRelease(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpInform(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    # client-side Handle methods
    def HandleDhcpOffer(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpAck(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpNack(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    # Handle unknown options or all options
    def HandleDhcpUnknown(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass

    def HandleDhcpAll(self, packet: dhcp_packet.DhcpPacket) -> None:
        pass


class DhcpServer(DhcpNetwork):
    def __init__(
        self, listen_address="0.0.0.0", client_listen_port=68, server_listen_port=67
    ) -> None:

        DhcpNetwork.__init__(
            self, listen_address, server_listen_port, client_listen_port
        )

        self.EnableBroadcast()
        self.DisableReuseaddr()

        self.CreateSocket()
        self.BindToAddress()


class DhcpClient(DhcpNetwork):
    def __init__(
        self, listen_address="0.0.0.0", client_listen_port=68, server_listen_port=67
    ) -> None:

        DhcpNetwork.__init__(
            self, listen_address, client_listen_port, server_listen_port
        )

        self.EnableBroadcast()
        self.EnableReuseaddr()

        self.CreateSocket()
