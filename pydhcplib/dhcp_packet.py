# pydhcplib
# Copyright (C) 2008 Mathieu Ignacio -- mignacio@april.org
#
# This file is part of pydhcplib.
# Pydhcplib is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from .dhcp_basic_packet import DhcpBasicPacket
from .dhcp_constants import (
    DhcpNames,
    DhcpFields,
    DhcpFieldsName,
    DhcpFieldsTypes,
    DhcpOptions,
    DhcpOptionsTypes,
    DhcpOptionsList,
)
import struct
from ipaddress import IPv4Address
from typing import List, Optional, Dict, Iterable, Mapping

from .type_hwmac import hwmac

# because of shadowing
str_ = str

class DhcpPacket(DhcpBasicPacket):
    def str(self) -> str_:
        # Process headers :
        printable_data = "# Header fields\n"

        op = self.packet_data[
            DhcpFields["op"][0] : DhcpFields["op"][0] + DhcpFields["op"][1]
        ]
        printable_data += "op : " + DhcpFieldsName["op"][str(op[0])] + "\n"

        for opt in [
            "htype",
            "hlen",
            "hops",
            "xid",
            "secs",
            "flags",
            "ciaddr",
            "yiaddr",
            "siaddr",
            "giaddr",
            "chaddr",
            "sname",
            "file",
        ]:
            begin = DhcpFields[opt][0]
            end = DhcpFields[opt][0] + DhcpFields[opt][1]
            data = bytes(self.packet_data[begin:end])
            result = ""
            if DhcpFieldsTypes[opt] == "int":
                result = str(data[0])
            elif DhcpFieldsTypes[opt] == "int2":
                result = str(struct.unpack(">H", data)[0])
            elif DhcpFieldsTypes[opt] == "int4":
                result = str(struct.unpack(">I", data)[0])
            elif DhcpFieldsTypes[opt] == "str":
                for each in data:
                    if each != 0:
                        result += chr(each)
                    else:
                        break

            elif DhcpFieldsTypes[opt] == "ipv4":
                result = str(IPv4Address(data))
            elif DhcpFieldsTypes[opt] == "hwmac":
                result = hwmac(data[:6]).str()

            printable_data += opt + " : " + result + "\n"

        # Process options :
        printable_data += "# Options fields\n"

        for opt in list(self.options_data.keys()):
            data = bytes(self.options_data[opt])
            result = ""
            optnum = DhcpOptions[opt]
            if opt == "dhcp_message_type":
                result = DhcpFieldsName["dhcp_message_type"][str(data[0])]
            elif DhcpOptionsTypes[optnum] == "char":
                result = str(data[0])
            elif DhcpOptionsTypes[optnum] == "16-bits":
                result = str(struct.unpack(">H", data)[0])
            elif DhcpOptionsTypes[optnum] == "32-bits":
                result = str(struct.unpack(">I", data)[0])
            elif DhcpOptionsTypes[optnum] == "string":
                for each in data:
                    if each != 0:
                        result += chr(each)
                    else:
                        break

            elif DhcpOptionsTypes[optnum] == "ipv4":
                result = str(IPv4Address(data))
            elif DhcpOptionsTypes[optnum] == "ipv4+":
                for i in range(0, len(data), 4):
                    if len(data[i: i + 4]) == 4:
                        result += str(IPv4Address(data[i: i + 4])) + " - "
            elif DhcpOptionsTypes[optnum] == "char+":
                if optnum == 55:  # parameter_request_list
                    result = ",".join([DhcpOptionsList[each] for each in data])
                else:
                    result += str(data)
            else:
                result += repr(data)

            printable_data += opt + " : " + result + "\n"

        return printable_data

    def AddLine(self, _string: str_) -> None:
        parameter, _, value_str = _string.partition(":")
        parameter = parameter.strip()
        # If value begin with a whitespace, remove it, leave others
        if len(value_str) > 0 and value_str[0] == " ":
            value_str = value_str[1:]
        value = self._OptionsToBinary(parameter, value_str)
        if value:
            self.SetOption(parameter, bytes(value))

    def _OptionsToBinary(self, parameter: str_, value: str_) -> Optional[List[int]]:
        # Transform textual data into dhcp binary data

        p = parameter.strip()
        # 1- Search for header informations or specific parameter
        if p == "op" or p == "htype":
            value = value.strip()
            if value.isdigit():
                return [int(value)]
            try:
                return [DhcpNames[value.strip()]]
            except KeyError:
                return [0]

        elif p == "hlen" or p == "hops":
            try:
                return [int(value)]
            except ValueError:
                return [0]

        elif p == "secs" or p == "flags":
            return list(int(value).to_bytes(2, "big"))

        elif p == "xid":
            return list(int(value).to_bytes(4, "big"))

        elif p == "ciaddr" or p == "yiaddr" or p == "siaddr" or p == "giaddr":
            try:
                return list(IPv4Address(value).packed)
            except ValueError:
                return [0, 0, 0, 0]

        elif p == "chaddr":
            try:
                return hwmac(value).list() + [0] * 10
            except (ValueError, TypeError):
                return [0] * 16

        elif p == "sname":
            return None
        elif p == "file":
            return None
        elif p == "parameter_request_list":
            params = value.strip().split(",")
            tmp = []
            for each in params:
                if each in DhcpOptions:
                    tmp.append(DhcpOptions[each])
            return tmp
        elif p == "dhcp_message_type":
            try:
                return [DhcpNames[value]]
            except KeyError:
                return None

        # 2- Search for options
        try:
            option_type = DhcpOptionsTypes[DhcpOptions[parameter]]
        except KeyError:
            return None

        if option_type == "ipv4":
            # this is a single ip address
            try:
                binary_value = list(map(int, value.split(".")))
            except ValueError:
                return None

        elif option_type == "ipv4+":
            # this is multiple ip address
            iplist = value.split(",")
            opt = []  # type: List[int]
            for single in iplist:
                opt += list(IPv4Address(single).packed)
            binary_value = opt

        elif option_type == "32-bits":
            # This is probably a number...
            try:
                digit = int(value)
                binary_value = [
                    digit >> 24 & 0xFF,
                    (digit >> 16) & 0xFF,
                    (digit >> 8) & 0xFF,
                    digit & 0xFF,
                ]
            except ValueError:
                return None

        elif option_type == "16-bits":
            try:
                digit = int(value)
                binary_value = [(digit >> 8) & 0xFF, digit & 0xFF]
            except ValueError:
                return None

        elif option_type == "char":
            try:
                digit = int(value)
                binary_value = [digit & 0xFF]
            except ValueError:
                return None

        elif option_type == "bool":
            if value == "False" or value == "false" or value == 0:
                binary_value = [0]
            else:
                binary_value = [1]

        elif option_type == "string":
            binary_value = list(value.encode("ascii"))

        else:
            raise ValueError(
                "could not determine encoding for {!r}".format(option_type))

        return binary_value

    # FIXME: This is called from IsDhcpSomethingPacket, but is this really
    # needed?  Or maybe this testing should be done in
    # DhcpBasicPacket.DecodePacket().

    # Test Packet Type
    def IsDhcpSomethingPacket(self, type_: List[int]) -> bool:
        if self.IsDhcpPacket() == False:
            return False
        if self.IsOption("dhcp_message_type") == False:
            return False
        if self.GetOption("dhcp_message_type") != bytes(type_):
            return False
        return True

    def IsDhcpDiscoverPacket(self) -> bool:
        return self.IsDhcpSomethingPacket([1])

    def IsDhcpOfferPacket(self) -> bool:
        return self.IsDhcpSomethingPacket([2])

    def IsDhcpRequestPacket(self) -> bool:
        return self.IsDhcpSomethingPacket([3])

    def IsDhcpDeclinePacket(self) -> bool:
        return self.IsDhcpSomethingPacket([4])

    def IsDhcpAckPacket(self) -> bool:
        return self.IsDhcpSomethingPacket([5])

    def IsDhcpNackPacket(self) -> bool:
        return self.IsDhcpSomethingPacket([6])

    def IsDhcpReleasePacket(self) -> bool:
        return self.IsDhcpSomethingPacket([7])

    def IsDhcpInformPacket(self) -> bool:
        return self.IsDhcpSomethingPacket([8])

    def GetMultipleOptions(self, options: Iterable[str_] = []) -> Dict[str_, bytes]:
        result = {}
        for each in options:
            result[each] = self.GetOption(each)
        return result

    def SetMultipleOptions(self, options: Mapping[str_, bytes] = {}) -> None:
        for each in list(options.keys()):
            self.SetOption(each, options[each])

    # Creating Response Packet

    # Server-side functions
    # From RFC 2132 page 28/29
    def CreateDhcpOfferPacketFrom(self, src: "DhcpPacket") -> None:  # src = discover packet
        self.SetOption("htype", src.GetOption("htype"))
        self.SetOption("xid", src.GetOption("xid"))
        self.SetOption("flags", src.GetOption("flags"))
        self.SetOption("giaddr", src.GetOption("giaddr"))
        self.SetOption("chaddr", src.GetOption("chaddr"))
        self.SetOption("ip_address_lease_time", src.GetOption("ip_address_lease_time"))
        self.TransformToDhcpOfferPacket()

    def TransformToDhcpOfferPacket(self) -> None:
        self.SetOption("dhcp_message_type", [2])
        self.SetOption("op", [2])
        self.SetOption("hlen", [6])

        self.DeleteOption("secs")
        self.DeleteOption("ciaddr")
        self.DeleteOption("request_ip_address")
        self.DeleteOption("parameter_request_list")
        self.DeleteOption("client_identifier")
        self.DeleteOption("maximum_message_size")

    """ Dhcp ACK packet creation """

    def CreateDhcpAckPacketFrom(self, src: "DhcpPacket") -> None:  # src = request or inform packet
        self.SetOption("htype", src.GetOption("htype"))
        self.SetOption("xid", src.GetOption("xid"))
        self.SetOption("ciaddr", src.GetOption("ciaddr"))
        self.SetOption("flags", src.GetOption("flags"))
        self.SetOption("giaddr", src.GetOption("giaddr"))
        self.SetOption("chaddr", src.GetOption("chaddr"))
        self.SetOption(
            "ip_address_lease_time_option",
            src.GetOption("ip_address_lease_time_option"),
        )
        self.TransformToDhcpAckPacket()

    def TransformToDhcpAckPacket(self) -> None:  # src = request or inform packet
        self.SetOption("op", [2])
        self.SetOption("hlen", [6])
        self.SetOption("dhcp_message_type", [5])

        self.DeleteOption("secs")
        self.DeleteOption("request_ip_address")
        self.DeleteOption("parameter_request_list")
        self.DeleteOption("client_identifier")
        self.DeleteOption("maximum_message_size")

    """ Dhcp NACK packet creation """

    def CreateDhcpNackPacketFrom(self, src: "DhcpPacket") -> None:  # src = request or inform packet

        self.SetOption("htype", src.GetOption("htype"))
        self.SetOption("xid", src.GetOption("xid"))
        self.SetOption("flags", src.GetOption("flags"))
        self.SetOption("giaddr", src.GetOption("giaddr"))
        self.SetOption("chaddr", src.GetOption("chaddr"))
        self.TransformToDhcpNackPacket()

    def TransformToDhcpNackPacket(self) -> None:
        self.SetOption("op", [2])
        self.SetOption("hlen", [6])
        self.DeleteOption("secs")
        self.DeleteOption("ciaddr")
        self.DeleteOption("yiaddr")
        self.DeleteOption("siaddr")
        self.DeleteOption("sname")
        self.DeleteOption("file")
        self.DeleteOption("request_ip_address")
        self.DeleteOption("ip_address_lease_time_option")
        self.DeleteOption("parameter_request_list")
        self.DeleteOption("client_identifier")
        self.DeleteOption("maximum_message_size")
        self.SetOption("dhcp_message_type", [6])

    """ GetClientIdentifier """

    def GetClientIdentifier(self) -> bytes:
        if self.IsOption("client_identifier"):
            return self.GetOption("client_identifier")
        return bytes()

    def GetGiaddr(self) -> bytes:
        return self.GetOption("giaddr")

    def GetHardwareAddress(self) -> bytes:
        length = self.GetOption("hlen")[0]
        full_hw = self.GetOption("chaddr")
        if length != [] and length < len(full_hw):
            return full_hw[0:length]
        return full_hw
